//
//  ResCollectionViewCell.swift
//  GGMap
//
//  Created by Nguyễn Xuân Nam on 10/17/19.
//  Copyright © 2019 Nguyễn Xuân Nam. All rights reserved.
//

import UIKit

class ResCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgResCl: UIImageView!
    
    @IBOutlet weak var lblNameResCl: UILabel!
    
}
