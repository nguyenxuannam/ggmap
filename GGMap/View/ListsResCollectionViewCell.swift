//
//  ListsResCollectionViewCell.swift
//  GGMap
//
//  Created by Nguyễn Xuân Nam on 10/17/19.
//  Copyright © 2019 Nguyễn Xuân Nam. All rights reserved.
//

import UIKit

class ListsResCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblNameRes: UILabel!
    
    @IBOutlet weak var imgListsRes: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        imgListsRes.layer.cornerRadius = 5
    }
}
