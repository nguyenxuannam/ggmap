//
//  RestaurantsTableViewCell.swift
//  GGMap
//
//  Created by Nguyễn Xuân Nam on 10/17/19.
//  Copyright © 2019 Nguyễn Xuân Nam. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {

    @IBOutlet weak var orderRes: UILabel!
    
    @IBOutlet weak var nameRes: UILabel!
    
    @IBOutlet weak var typeOfRes: UILabel!
    
    @IBOutlet weak var imgRes: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
