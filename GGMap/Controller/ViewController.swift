//
//  ViewController.swift
//  GGMap
//
//  Created by Nguyễn Xuân Nam on 10/17/19.
//  Copyright © 2019 Nguyễn Xuân Nam. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController{
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var resTableView: UITableView!
    @IBOutlet weak var resCollectionView: UICollectionView!
    @IBOutlet weak var listsResCollectionView: UICollectionView!
    
    @IBOutlet weak var uiView: UIView!
    
    let locationManager = CLLocationManager()
    
    let nameResCl: [String] = ["Nhà hàng", "Nhà hàng", "Nhà hàng", "Nhà hàng"]
    let imgResCl: [UIImage] = [UIImage(named: "eat")!, UIImage(named: "eat")!, UIImage(named: "eat")!, UIImage(named: "eat")!]
    
    let listNameRes: [String] = ["Quuán bar 1", "Quuán bar 2", "Quuán bar 3", "Quuán bar 4", "Quuán bar 5", "Quuán bar 6"]
    let imgListNameRes: [UIImage] = [UIImage(named: "res1")!, UIImage(named: "res2")!, UIImage(named: "res3")!, UIImage(named: "res1")!, UIImage(named: "res2")!, UIImage(named: "res3")!]
    
    let nameRes: [String] = ["Nhà hàng 1", "Nhà hàng 3", "Nhà hàng 3"]
    let imgRes: [UIImage] = [UIImage(named: "res1")!, UIImage(named: "res2")!, UIImage(named: "res3")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resCollectionView.dataSource = self
        resCollectionView.delegate = self
        
        listsResCollectionView.dataSource = self
        listsResCollectionView.delegate = self
        
        resTableView.dataSource = self
        resTableView.delegate = self
        
        tabBarController?.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    @IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.uiView)
        let vel = sender.velocity(in: self.uiView)
        
        if sender.state == .ended && vel.y >= 0 {
            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: { self.uiView.center = CGPoint(x: self.view.center.x, y: self.view.center.y + self.uiView.frame.height/1.6)}, completion: nil)
        } else if sender.state == .ended && vel.y < 0 {
            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: { self.uiView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 15)}, completion: nil)
        } else if sender.state == .changed {
            if let view = sender.view {
                view.center = CGPoint(x:view.center.x, y:view.center.y + translation.y)
            }
            sender.setTranslation(CGPoint.zero, in: self.uiView)
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
}

extension ViewController: UICollectionViewDelegate {
    
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.listsResCollectionView {
            return listNameRes.count
        } else {
            return nameResCl.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.listsResCollectionView {
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "LISTSRESCELL", for: indexPath) as! ListsResCollectionViewCell
            
            cell1.lblNameRes.text = listNameRes[indexPath.row]
            cell1.imgListsRes.image = imgListNameRes[indexPath.row]
            
            return cell1
        } else {
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "RESCELL", for: indexPath) as! ResCollectionViewCell
            
            cell2.lblNameResCl.text = nameResCl[indexPath.row]
            cell2.imgResCl.image = imgResCl[indexPath.row]
            
            return cell2
        }
    }
}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nameRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RESCELL", for: indexPath) as! RestaurantsTableViewCell
        
        cell.orderRes.text = "\(indexPath.row + 1)"
        cell.nameRes.text = nameRes[indexPath.row]
        cell.imgRes.image = imgRes[indexPath.row]
        
        return cell
    }
}

extension ViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: { self.uiView.center = CGPoint(x: self.view.center.x, y: self.view.center.y + self.uiView.frame.height/1.6)}, completion: nil)
    }
}
